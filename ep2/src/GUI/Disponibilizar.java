/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import ep2.Ep2;

/**
 *
 * @author alvar
 */
public class Disponibilizar extends javax.swing.JInternalFrame {

    /**
     * Creates new form Conferir
     */
    public Disponibilizar() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Logo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        CarretaD = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        VansD = new javax.swing.JLabel();
        CarrosD = new javax.swing.JLabel();
        MotosD = new javax.swing.JLabel();
        CarroRmV = new javax.swing.JButton();
        CarretasRmV = new javax.swing.JButton();
        MotoRmV = new javax.swing.JButton();
        VanRmV = new javax.swing.JButton();

        setBorder(null);
        setClosable(true);

        Logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Logo.png"))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Agency FB", 0, 24)); // NOI18N
        jLabel1.setText("MOTOS:");

        jLabel2.setFont(new java.awt.Font("Agency FB", 0, 24)); // NOI18N
        jLabel2.setText("INDISPONIVEL");

        CarretaD.setFont(new java.awt.Font("Agency FB", 0, 36)); // NOI18N
        CarretaD.setText("0");

        jButton1.setText("Checar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Agency FB", 0, 24)); // NOI18N
        jLabel5.setText("CARRETAS:");

        jLabel6.setFont(new java.awt.Font("Agency FB", 0, 24)); // NOI18N
        jLabel6.setText("VANS:");

        jLabel7.setFont(new java.awt.Font("Agency FB", 0, 24)); // NOI18N
        jLabel7.setText("CARROS:");

        VansD.setFont(new java.awt.Font("Agency FB", 0, 36)); // NOI18N
        VansD.setText("0");

        CarrosD.setFont(new java.awt.Font("Agency FB", 0, 36)); // NOI18N
        CarrosD.setText("0");

        MotosD.setFont(new java.awt.Font("Agency FB", 0, 36)); // NOI18N
        MotosD.setText("0");

        CarroRmV.setText("Disponibilizar");
        CarroRmV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CarroRmVActionPerformed(evt);
            }
        });

        CarretasRmV.setText("Disponibilizar");
        CarretasRmV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CarretasRmVActionPerformed(evt);
            }
        });

        MotoRmV.setText("Disponibilizar");
        MotoRmV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MotoRmVActionPerformed(evt);
            }
        });

        VanRmV.setText("Disponibilizar");
        VanRmV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VanRmVActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                                .addComponent(CarrosD))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(CarretaD))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(MotosD))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(VansD)))
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(CarroRmV)
                            .addComponent(CarretasRmV)
                            .addComponent(MotoRmV)
                            .addComponent(VanRmV))
                        .addGap(8, 8, 8))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(12, 12, 12))
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(Logo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Logo)
                .addGap(18, 36, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(CarretaD, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(CarretasRmV)))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(VansD, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(VanRmV)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CarrosD, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                    .addComponent(jLabel7)
                    .addComponent(CarroRmV))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(MotosD, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                        .addGap(5, 5, 5))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(MotoRmV)
                        .addGap(25, 25, 25))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int CarT = 0, MotoT = 0, VanT = 0, CarretaT = 0;
        int i;
        
        for (i = 0; i < Ep2.Lista.size(); i++) {
            if(Ep2.Lista.get(i).getDisponibilidade() == 'I'){
                switch(Ep2.Lista.get(i).getTipo()){
                    case "Moto":
                        MotoT++;
                        break;
                    case "Carro":
                        CarT++;
                        break;
                    case "Van":
                        VanT++;
                        break;
                    case "Carreta":
                        CarretaT++;
                        break;
                }
            }
        }
        CarretaD.setText(CarretaT+"");
        VansD.setText(VanT+"");;
        CarrosD.setText(CarT+"");
        MotosD.setText(MotoT+"");
        
        if(MotoT == 0){
            MotoRmV.setText("Indisponivel");
        }
        if(CarT == 0){
            CarroRmV.setText("Indisponivel");
        }
        if(VanT == 0){
            VanRmV.setText("Indisponivel");
        }
        if(CarretaT == 0){
            CarretasRmV.setText("Indisponivel");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void CarroRmVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CarroRmVActionPerformed
        int i, QCarros = Integer.parseInt(CarrosD.getText());
        
        if(QCarros > 0){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'I' && Ep2.Lista.get(i).getTipo().equals("Carro")){
                    Ep2.Lista.get(i).setDisponibilidade('D');
                    break;
                }
            }
            Ep2.adicionaTxt();
            QCarros--;
            CarrosD.setText(QCarros+"");
            if(QCarros == 0){
                CarroRmV.setText("Indisponivel");
            }
        }
    }//GEN-LAST:event_CarroRmVActionPerformed

    private void CarretasRmVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CarretasRmVActionPerformed
        int i, QCarreta = Integer.parseInt(CarretaD.getText());
        
        if(QCarreta > 0){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'I' && Ep2.Lista.get(i).getTipo().equals("Carreta")){
                    Ep2.Lista.get(i).setDisponibilidade('D');
                    break;
                }
            }
            Ep2.adicionaTxt();
            QCarreta--;
            CarretaD.setText(QCarreta+"");
            if(QCarreta == 0){
                CarretasRmV.setText("Indisponivel");
            }
        }
    }//GEN-LAST:event_CarretasRmVActionPerformed

    private void VanRmVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VanRmVActionPerformed
    int i, QVan = Integer.parseInt(VansD.getText());
        
        if(QVan > 0){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'I' && Ep2.Lista.get(i).getTipo().equals("Van")){
                    Ep2.Lista.get(i).setDisponibilidade('D');
                    break;
                }
            }
            Ep2.adicionaTxt();
            QVan--;
            VansD.setText(QVan+"");
            if(QVan == 0){
                VanRmV.setText("Indisponivel");
            }
        }
    }//GEN-LAST:event_VanRmVActionPerformed

    private void MotoRmVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MotoRmVActionPerformed
        int i, QMoto = Integer.parseInt(MotosD.getText());
        
        if(QMoto > 0){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'I' && Ep2.Lista.get(i).getTipo().equals("Moto")){
                    Ep2.Lista.get(i).setDisponibilidade('D');
                    break;
                }
            }
            Ep2.adicionaTxt();
            QMoto--;
            MotosD.setText(QMoto+"");
            if(QMoto == 0){
                MotoRmV.setText("Indisponivel");
            }
        }
    }//GEN-LAST:event_MotoRmVActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel CarretaD;
    private javax.swing.JButton CarretasRmV;
    private javax.swing.JButton CarroRmV;
    private javax.swing.JLabel CarrosD;
    private javax.swing.JLabel Logo;
    private javax.swing.JButton MotoRmV;
    private javax.swing.JLabel MotosD;
    private javax.swing.JButton VanRmV;
    private javax.swing.JLabel VansD;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JSeparator jSeparator3;
    // End of variables declaration//GEN-END:variables
}
