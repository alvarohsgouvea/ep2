/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import ep2.Entrega;
import ep2.Ep2;
import static ep2.Ep2.Gas;
import java.text.DecimalFormat;

/**
 *
 * @author alvar
 */
public class NovasEntregas extends javax.swing.JInternalFrame {

    /**
     * Creates new form NovasEntregas
     */
    public NovasEntregas() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        Peso = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        Distancia = new javax.swing.JTextField();
        Tempo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Gerar = new javax.swing.JButton();
        SuccesReport = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        ComboBoxVeiculos = new javax.swing.JComboBox<>();
        Inicializar = new javax.swing.JButton();
        CustoLabel = new javax.swing.JLabel();
        Tipo = new javax.swing.JLabel();
        TempoLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        SuccessEntrega = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createCompoundBorder());
        setClosable(true);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setText("Peso Da Carga:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setText("Distancia da Entrega:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setText("Tempo de Entrega:");

        Gerar.setText("Checar");
        Gerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GerarActionPerformed(evt);
            }
        });

        SuccesReport.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Kg");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Km");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Horas");

        ComboBoxVeiculos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione" }));
        ComboBoxVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBoxVeiculosActionPerformed(evt);
            }
        });

        Inicializar.setText("Iniciar Entrega");
        Inicializar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InicializarActionPerformed(evt);
            }
        });

        CustoLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        CustoLabel.setText("Lucro");

        Tipo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Tipo.setText("Tipo");

        TempoLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        TempoLabel.setText("Tempo");

        SuccessEntrega.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SuccesReport, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Gerar))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Distancia, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(CustoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Peso, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Tempo, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(72, 72, 72)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ComboBoxVeiculos, 0, 162, Short.MAX_VALUE)
                                    .addComponent(Tipo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(TempoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(SuccessEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Inicializar, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(19, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Inicializar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                    .addComponent(ComboBoxVeiculos))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Peso, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Tipo, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TempoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Distancia, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(CustoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(SuccessEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Tempo, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Gerar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(SuccesReport, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void GerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GerarActionPerformed
        int i, Moto = 0, Carro = 0, Vans = 0, Carreta = 0, Disponiveis = 0;
        float PesoCarga = Integer.parseInt(Peso.getText());
        float Dist = Integer.parseInt(Distancia.getText());
        float Time = Integer.parseInt(Tempo.getText());
        
        for (i = 0; i < Ep2.Lista.size(); i++) {
            if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                Disponiveis++;
                switch(Ep2.Lista.get(i).getTipo()){
                    case "Moto":
                        Moto++;
                    break;
                    case "Carro":
                        Carro++;
                    break;
                    case "Van":
                        Vans++;
                    break;
                    case "Carreta":
                        Carreta++;
                        break;
                }
            }
        }
        
        if(Dist/Time > 110){
            PesoCarga = 40000;
        }
        if(Moto == 0){
            if(Dist/Time > 100){
                PesoCarga = 40000;
            }
        }else{
            if(Dist/Time > 100 && PesoCarga > 50){
                PesoCarga = 40000;
            }
        }
        if(PesoCarga > 50){
            if(Carro == 0){
                if(Dist/Time > 80){
                    PesoCarga = 40000;
                }
            }else{
                if(Dist/Time > 80 && PesoCarga > 360){
                    PesoCarga = 40000;
                }
            }
        }
        if(PesoCarga > 360){
            if(Vans == 0){
                if(Dist/Time > 60){
                    PesoCarga = 40000;
                }
            }else{
                if(Dist/Time > 60 && PesoCarga > 3500){
                    PesoCarga = 40000;
                }
            }
        }
        if(PesoCarga > 3500){
            if(Carreta == 0){
                PesoCarga = 40000;
            }else{
                if(Dist/Time > 60){
                    PesoCarga = 40000;
                }
            }
        }
        
        
        if(Disponiveis == 0 || PesoCarga > 30000){
            Tipo.setText("Tipo");
            TempoLabel.setText("Tempo");
            CustoLabel.setText("Lucro");
            SuccessEntrega.setText("");
            ComboBoxVeiculos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione"}));
            SuccesReport.setText("Entrega Recusada!");
        }else if(Disponiveis != 0 && PesoCarga <= 30000){
            Tipo.setText("");
            TempoLabel.setText("");
            CustoLabel.setText("");
            SuccessEntrega.setText("");
            SuccesReport.setText("");
            ComboBoxVeiculos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione", "Menor Custo", "Menor Tempo", "Custo Beneficio"}));
        }
    }//GEN-LAST:event_GerarActionPerformed

    private void ComboBoxVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBoxVeiculosActionPerformed
        int i, MotoG = 0, MotoA = 0, CarroG = 0, CarroA = 0, Vans = 0, Carreta = 0; 
        String Type = "";
        float Rendimento = 0, Custo = 0, KmH = 0, PercentLucro = (float) (1+(Ep2.Lucro/100));
        float PesoCarga = Integer.parseInt(Peso.getText());
        float Dist = Integer.parseInt(Distancia.getText());
        float Time = Integer.parseInt(Tempo.getText());
        
        DecimalFormat Formatar = new DecimalFormat("#.00");
        DecimalFormat Hora = new DecimalFormat("#.0");
        DecimalFormat Min = new DecimalFormat("#");

        for (i = 0; i < Ep2.Lista.size(); i++) {
            if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                switch(Ep2.Lista.get(i).getTipo()){
                    case "Moto":
                        if(Ep2.Lista.get(i).getComb() == 'G'){
                            MotoG++;
                        }else{
                            MotoA++;
                        }
                    break;
                    case "Carro":
                        if(Ep2.Lista.get(i).getComb() == 'G'){
                            CarroG++;
                        }else{
                            CarroA++;
                        }
                    break;
                    case "Van":
                        Vans++;
                    break;
                    case "Carreta":
                        Carreta++;
                        break;
                }
            }
        }
        
        float CustoMoto = -1, CustoCarro = -1, CustoVan = -1, CustoCar = -1, MenorCusto = -1;
        float RendMoto = 0, RendCarro = 0, RendVan = 0, RendCar = 0, MenorCustoTempo = 0;
        char CombMoto = ' ', CombCarro = ' ';
        
        
        if(PesoCarga > 50 || MotoG+MotoA == 0){
            CustoMoto = -1;
        }else{
            float CustA, CustG;
            
            CustG = (float) (Dist/(50-(0.3*PesoCarga)));
            CustG = (float) (CustG*4.449);
            
            CustA = (float) (Dist/(43-(0.4*PesoCarga)));
            CustA = (float) (CustA*3.499);
            
            if(MotoG == 0){
                CustoMoto = CustA;
                CombMoto = 'A';
            }else if(MotoA == 0){
                CustoMoto = CustG;
                CombMoto = 'G';
            }else{
                if(CustA > MotoG){
                    CustoMoto = CustG;
                    CombMoto = 'G';
                }else{
                    CustoMoto = CustA;
                    CombMoto = 'A';
                }
            }
        }
        if(PesoCarga > 360 || CarroG+CarroA == 0){
            CustoCarro = -1;
        }else{
            float CustA, CustG;
            
            CustG = (float) (Dist/(14-(0.025*PesoCarga)));
            CustG = (float) (CustG*4.449);
            
            CustA = (float) (Dist/(12-(0.0231*PesoCarga)));
            CustA = (float) (CustA*3.499);

            if(CarroG == 0){
                CustoCarro = CustA;
                CombCarro = 'A';
            }else if(CarroA == 0){
                CustoCarro = CustG;
                CombCarro = 'G';
            }else{
                if(CustA > CarroG){
                    CustoCarro = CustG;
                    CombCarro = 'G';
                }else{
                    CustoCarro = CustA;
                    CombCarro = 'A';
                }
            }
            
        }
        if(PesoCarga > 3500 || Vans == 0){
            CustoVan = -1;
        }else{
            CustoVan = (float) (Dist/(10-(0.001*PesoCarga)));
            CustoVan = (float) (CustoVan*3.869);
        }
        if(PesoCarga > 30000 || Carreta == 0){
            CustoCar = -1;
        }else{
            CustoCar = (float) (Dist/(8-(0.0002*PesoCarga)));
            CustoCar = (float) (CustoCar*3.869);
        }
        
        if(PesoCarga > 30000 || (CustoMoto == -1 && CustoCarro == -1 && CustoVan == -1 && CustoCar == -1)){
            SuccesReport.setText("Entrega Recusada!");
            Peso.setText("");
            Distancia.setText("");
            Tempo.setText("");
            ComboBoxVeiculos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione"}));
        }else{
            SuccesReport.setText("");
            if(ComboBoxVeiculos.getSelectedItem().equals("Menor Custo")){
                ////////////////////////////////////////////////////////
                if(CustoMoto != -1){
                    MenorCusto = CustoMoto;
                    Ep2.Type = "Moto";
                    Ep2.Gas = CombMoto;
                    KmH = Dist/110;
                }
                if(CustoCarro != -1){
                    if(MenorCusto == -1 || CustoCarro < MenorCusto){
                        MenorCusto = CustoCarro;
                        Ep2.Type = "Carro";
                        KmH = Dist/100;
                        Ep2.Gas = CombCarro;
                    }
                }
                if(CustoVan != -1){
                    if(MenorCusto == -1 || CustoVan < MenorCusto){
                        MenorCusto = CustoVan;
                        Ep2.Type = "Van";
                        KmH = Dist/80;
                        Ep2.Gas = 'D';
                    }
                }
                if(CustoCar != -1){
                    if(MenorCusto == -1 || CustoCar < MenorCusto){
                        MenorCusto = CustoCar;
                        Ep2.Type = "Carreta";
                        KmH = Dist/60;
                        Ep2.Gas = 'D';
                    }
                }
                MenorCusto = ((float) MenorCusto - (float) (MenorCusto/PercentLucro));
                Tipo.setText(Ep2.Type+"");
                if(KmH > 1){
                    TempoLabel.setText(Hora.format(KmH)+" Hora(s)");
                }else{
                    KmH*=60;
                    TempoLabel.setText(Min.format(KmH)+" Minuto(s)");
                }
                CustoLabel.setText(Formatar.format(MenorCusto)+" Reais"); 
                ////////////////////////////////////////////////////
            }else if(ComboBoxVeiculos.getSelectedItem().equals("Menor Tempo")){
                ////////////////////////////////////////////////////
                if(CustoMoto != -1){
                    Custo = CustoMoto;
                    Ep2.Type = "Moto";
                    KmH = Dist/110;
                    Ep2.Gas = CombMoto;
                }else if (CustoCarro != -1){
                    Custo = CustoCarro;
                    Ep2.Type = "Carro";
                    KmH = Dist/100;
                    Ep2.Gas = CombCarro;
                }else if(CustoVan != -1){
                    Custo = CustoVan;
                    Ep2.Type = "Van";
                    KmH = Dist/80;
                    Ep2.Gas = 'D';
                }else if(CustoCar != -1){
                    Custo = CustoCar;
                    Ep2.Type = "Carreta";
                    KmH = Dist/60;
                    Ep2.Gas = 'D';
                }
                Custo = ((float) Custo - (float) (Custo/PercentLucro));
                Tipo.setText(Ep2.Type+"");
                if(KmH > 1){
                    TempoLabel.setText(Hora.format(KmH)+" Hora(s)");
                }else{
                    KmH*=60;
                    TempoLabel.setText(Min.format(KmH)+" Minuto(s)");
                }
                CustoLabel.setText(Formatar.format(Custo)+" Reais");
                ////////////////////////////////////////////////////
            }else if(ComboBoxVeiculos.getSelectedItem().equals("Custo Beneficio")){
                ////////////////////////////////////////////////////
                float CBMoto = -1, CBCarro = -1, CBVan = -1, CBCar = -1, CB = -1;
                
                if(CustoMoto != -1){
                    CustoMoto =  (float) (CustoMoto - (CustoMoto/PercentLucro));
                    CBMoto = CustoMoto/(Dist/110);
                }
                if(CustoCarro != -1){
                    CustoCarro =  (float) (CustoCarro - (CustoCarro/PercentLucro));
                    CBCarro = CustoCarro/(Dist/100);
                }
                if(CustoVan != -1){
                    CustoVan =  (float) (CustoVan - (CustoVan/PercentLucro));
                    CBVan = CustoVan/(Dist/80);
                }
                if(CustoCar != -1){
                    CustoCar =  (float) (CustoCar - (CustoCar/PercentLucro));
                    CBCar = CustoCar/(Dist/60);
                }
                
                if(CBMoto != 1){
                    CB = CBMoto;
                    Custo = CustoMoto;
                    Ep2.Type = "Moto";
                    KmH = Dist/110;
                    Ep2.Gas = CombMoto;
                }
                if(CBCarro != -1){
                    if(CB == -1 || CBCarro > CB){
                        CB = CBCarro;
                        Custo = CustoCarro;
                        Ep2.Type = "Carro";
                        KmH = Dist/100;
                        Ep2.Gas = CombCarro;
                    }
                }
                if(CBVan != -1){
                    if(CB == -1 || CBVan > CB){
                        CB = CBVan;
                        Custo = CustoVan;
                        Ep2.Type = "Van";
                        KmH = Dist/80;
                        Ep2.Gas = 'D';
                    }
                }
                if(CBCar != -1){
                    if(CB == -1 || CBCar > CB){
                        CB = CBCar;
                        Custo = CustoCar;
                        Ep2.Type = "Carreta";
                        KmH = Dist/60;
                        Ep2.Gas = 'D';
                    }
                }
                
                
                Tipo.setText(Ep2.Type+"");
                if(KmH > 1){
                    TempoLabel.setText(Hora.format(KmH)+" Hora(s)");
                }else{
                    KmH*=60;
                    TempoLabel.setText(Min.format(KmH)+" Minuto(s)");
                }
                CustoLabel.setText(Formatar.format(Custo)+" Reais"); 
            }
        }
    }//GEN-LAST:event_ComboBoxVeiculosActionPerformed

    private void InicializarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InicializarActionPerformed
        int i;
        int PesoCarga = Integer.parseInt(Peso.getText());
        int Dist = Integer.parseInt(Distancia.getText());
        String Veiculo = Tipo.getText();
        String Reais = CustoLabel.getText(), Valor;
        Valor = Reais.replace(" Reais", "");
        float Lucro = Float.parseFloat(Valor);
        
        SuccesReport.setText("");
        Tipo.setText("");
        TempoLabel.setText("");
        CustoLabel.setText("");
        Peso.setText("");
        Distancia.setText("");
        Tempo.setText("");
        if(ComboBoxVeiculos.getSelectedItem().equals("Menor Tempo")){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                   if(Ep2.Lista.get(i).getTipo().equals(Ep2.Type) && Ep2.Lista.get(i).getComb() == Ep2.Gas){
                       Ep2.Lista.get(i).setDisponibilidade('I');
                       break;
                    } 
                }
            }   
            SuccessEntrega.setText("Entrega Inicializada");
            Ep2.adicionaTxt();
            Entrega New = new Entrega();
            New.setEntrega(Veiculo, PesoCarga, Dist, Lucro);
            Ep2.newEntrega(New);
            Ep2.DeliveryTxt();
        }else if(ComboBoxVeiculos.getSelectedItem().equals("Menor Custo")){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                   if(Ep2.Lista.get(i).getTipo().equals(Ep2.Type) && Ep2.Lista.get(i).getComb() == Ep2.Gas){
                       Ep2.Lista.get(i).setDisponibilidade('I');
                       break;
                    } 
                }
            }
            Ep2.adicionaTxt();
            SuccessEntrega.setText("Entrega Inicializada");
            Entrega New = new Entrega();
            New.setEntrega(Veiculo, PesoCarga, Dist, Lucro);
            Ep2.newEntrega(New);
            Ep2.DeliveryTxt();
        }else if(ComboBoxVeiculos.getSelectedItem().equals("Custo Beneficio")){
            for (i = 0; i < Ep2.Lista.size(); i++) {
                if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                   if(Ep2.Lista.get(i).getTipo().equals(Ep2.Type) && Ep2.Lista.get(i).getComb() == Ep2.Gas){
                       Ep2.Lista.get(i).setDisponibilidade('I');
                       break;
                    } 
                }
            }
            Ep2.adicionaTxt();
            SuccessEntrega.setText("Entrega Inicializada");
            Entrega New = new Entrega();
            New.setEntrega(Veiculo, PesoCarga, Dist, Lucro);
            Ep2.newEntrega(New);
            Ep2.DeliveryTxt();
        }else{
            SuccessEntrega.setText("");
        }
    }//GEN-LAST:event_InicializarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> ComboBoxVeiculos;
    private javax.swing.JLabel CustoLabel;
    private javax.swing.JTextField Distancia;
    private javax.swing.JButton Gerar;
    private javax.swing.JButton Inicializar;
    private javax.swing.JTextField Peso;
    private javax.swing.JLabel SuccesReport;
    private javax.swing.JLabel SuccessEntrega;
    private javax.swing.JTextField Tempo;
    private javax.swing.JLabel TempoLabel;
    private javax.swing.JLabel Tipo;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables
}
