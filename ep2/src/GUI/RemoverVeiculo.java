/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import ep2.Ep2;

/**
 *
 * @author alvar
 */
public class RemoverVeiculo extends javax.swing.JInternalFrame {

    /**
     * Creates new form RemoverVeiculo
     */
    public RemoverVeiculo() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Remover = new javax.swing.JButton();
        Veiculo = new javax.swing.JComboBox<>();
        SuccessReport = new javax.swing.JLabel();
        Qtd = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createCompoundBorder());
        setClosable(true);

        jLabel1.setText("Só é possível remover veículos dísponiveis");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel6.setText("Excluir:");

        Remover.setText("Conferir");
        Remover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoverActionPerformed(evt);
            }
        });

        Veiculo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione" }));
        Veiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VeiculoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(SuccessReport, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Veiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Qtd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(Remover, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addContainerGap(112, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Qtd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Veiculo, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addGap(29, 29, 29)
                .addComponent(Remover, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(SuccessReport, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoverActionPerformed
        String Type = "";
        int i, Motos = 0, Carros = 0, Vans = 0, Carretas = 0;
        String AvaibleMoto = "Indisponivel", AvaibleCarro = "Indisponivel", AvaibleVan = "Indisponivel", AvaibleCarreta = "Indisponivel";
        if(Veiculo.getSelectedItem().equals("Selecione")){
            for (i = 0; i < Ep2.Lista.size(); i++){
                if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                    switch(Ep2.Lista.get(i).getTipo()){
                        case "Moto":
                            Motos++;
                            break;
                        case "Carro":
                            Carros++;
                            break;
                        case "Van":
                            Vans++;
                            break;
                        case "Carreta":
                            Carretas++;
                            break;
                    }
                }
            }
            if(Motos != 0){
                AvaibleMoto = "Moto";
            }
            if(Carros != 0){
                AvaibleCarro = "Carro";
            }
            if(Vans != 0){
                AvaibleVan = "Van";
            }
            if(Carretas != 0){
                AvaibleCarreta = "Carreta";
            }
            Remover.setText("Remover");
            Veiculo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione", AvaibleMoto, AvaibleCarro, AvaibleVan, AvaibleCarreta}));
        }else{
            int QTD = Integer.parseInt(Qtd.getText());
            if(Veiculo.getSelectedItem().equals("Moto")){
                Type = "Moto";
                Motos--;
            }else if(Veiculo.getSelectedItem().equals("Carro")){
                Type = "Carro";
                Carros--;
            }else if(Veiculo.getSelectedItem().equals("Van")){
                Type = "Van";
                Vans--;
            }else if(Veiculo.getSelectedItem().equals("Carreta")){
                Type = "Carreta";
                Carretas--;
            }
            if(QTD > 0){
                for (i = 0; i < Ep2.Lista.size(); i++) {
                    if(Ep2.Lista.get(i).getTipo().equals(Type) && Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                        Ep2.Lista.remove(i);
                        break;
                    }
                }
                SuccessReport.setText("Veiculo Excluido!");
                Qtd.setText((QTD-1)+"");
            }else{
                if(Motos == 0){
                    AvaibleMoto = "Indisponivel";
                }
                if(Carros == 0){
                    AvaibleCarro = "Indisponivel";
                }
                if(Vans == 0){
                    AvaibleVan = "Indisponivel";
                }
                if(Carretas == 0){
                    AvaibleCarreta = "Indisponivel";
                }
                SuccessReport.setText("");
                Veiculo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione", AvaibleMoto, AvaibleCarro, AvaibleVan, AvaibleCarreta}));
                Qtd.setText("");
            }
        }
        Ep2.adicionaTxt();
    }//GEN-LAST:event_RemoverActionPerformed

    private void VeiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VeiculoActionPerformed
       int i, Motos = 0, Carros = 0, Vans = 0, Carretas = 0;
        for (i = 0; i < Ep2.Lista.size(); i++){
            if(Ep2.Lista.get(i).getDisponibilidade() == 'D'){
                switch(Ep2.Lista.get(i).getTipo()){
                    case "Moto":
                        Motos++;
                    break;
                    case "Carro":
                        Carros++;
                    break;
                    case "Van":
                        Vans++;
                    break;
                    case "Carreta":
                        Carretas++;
                    break;
                }
            }
        }

        if(Veiculo.getSelectedItem().equals("Moto")){
                Qtd.setText(Motos+"");
        }else if(Veiculo.getSelectedItem().equals("Carro")){
                Qtd.setText(Carros+"");
        }else if(Veiculo.getSelectedItem().equals("Van")){
                Qtd.setText(Vans+"");
        }else if(Veiculo.getSelectedItem().equals("Carreta")){
                Qtd.setText(Carretas+"");
        }
    }//GEN-LAST:event_VeiculoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Qtd;
    private javax.swing.JButton Remover;
    private javax.swing.JLabel SuccessReport;
    private javax.swing.JComboBox<String> Veiculo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}
