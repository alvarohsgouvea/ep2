/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

import GUI.TelaPrincipal;
import java.awt.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;

/**
 *
 * @author alvar
 */
public class Ep2 {

    /**
     *
     */
    public static ArrayList<Entrega> Delivery = new ArrayList<>();
    public static ArrayList<Veiculos> Lista = new ArrayList<>();
    public static String Type = "";
    public static char Gas = ' ';
    public static double Lucro = 20;
    private static Formatter arq;
    private static Formatter ArqDelivery;
    private static Scanner GetArq;
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        File Car = new File("Carros.txt");
        File Entregas = new File("Entregas.txt");
        File lucro = new File("Lucro.txt");
        
        if(lucro.exists()){
            Scanner ScanArq;
            try{
                ScanArq = new Scanner(new File("Lucro.txt"));
                Lucro = Double.parseDouble(ScanArq.next());
                ScanArq.close();
            } catch (FileNotFoundException ex) {
                System.out.println("Erro de acesso ao arquivo");
            }
        }else{
            try{
                arq = new Formatter("Lucro.txt");
                arq.format("%f", 20.00);
                arq.close();
            } catch (FileNotFoundException ex) {
                System.out.println("Erro de acesso ao arquivo");
            }
        }
        
        
        if(Entregas.exists()){
            openReadDelivery();
        }else{
            DeliveryTxt();
        }
        
        if(Car.exists()){
            openRead();
        }else{
            adicionaTxt();
            
        }
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new TelaPrincipal().setVisible(true);
        });
    }
    
    private static void openFile(){
        try{
            arq = new Formatter("Carros.txt");
        } catch (FileNotFoundException ex) {
            System.out.println("Erro de acesso ao arquivo");
        }
    }
    
    public static void adicionaTxt(){
        int i;
        openFile();
        for (i = 0; i < Lista.size(); i++) {
           arq.format("%c %c %s\n", Lista.get(i).getComb(), Lista.get(i).getDisponibilidade(), Lista.get(i).getTipo());
        }
       arq.close();
    }
    
    private static void openRead(){
        String Combustivel, Disponibilidade, Type;
        char Comb, Disp;
        
        try{
            GetArq = new Scanner(new File("Carros.txt"));
        } catch (FileNotFoundException ex) {
            System.out.println("Erro de acesso ao arquivo");
        }
       
        while(GetArq.hasNext()){
            Combustivel = GetArq.next();
            Disponibilidade = GetArq.next();
            Type = GetArq.next();
            Comb = Combustivel.charAt(0);
            Disp = Disponibilidade.charAt(0);
            if(Type.equals("Carreta")){
                Carreta Carreta = new Carreta();
                Carreta.setCarreta();
                addCarreta(Carreta); 
                if(Disp == 'I'){
                    Carreta.setDisponibilidade('I');
                }
            }
            if(Type.equals("Van")){
                Van Van = new Van();
               Van.setVan();
                addVan(Van);
                if(Disp == 'I'){
                    Van.setDisponibilidade('I');
                }
            }
            if(Type.equals("Carro")){
                Carro Carro = new Carro();
                Carro.setCar(Comb);
                addCar(Carro);
                if(Disp == 'I'){
                    Carro.setDisponibilidade('I');
                }
            }
            if(Type.equals("Moto")){
                Moto Moto = new Moto();
                Moto.setMoto(Comb);
                addMoto(Moto);
                if(Disp == 'I'){
                    Moto.setDisponibilidade('I');
                }
            }
        }
    }
    
    public static void DeliveryTxt(){
        int i;
        
        try{
            ArqDelivery = new Formatter("Entregas.txt");
        } catch (FileNotFoundException ex) {
            System.out.println("Erro de acesso ao arquivo");
        }
        
        for (i = 0; i < Delivery.size(); i++) {
            ArqDelivery.format("%s %d %d %f\n", Delivery.get(i).getTipo(), Delivery.get(i).getPeso(), Delivery.get(i).getDistancia(), Delivery.get(i).getLucro());
        }
        ArqDelivery.close();
    }
    
    
    private static void openReadDelivery(){
        String Distancia, Peso, Type, Lucro;
        int peso, distancia;
        float lucro;
        
        try{
            GetArq = new Scanner(new File("Entregas.txt"));
        } catch (FileNotFoundException ex) {
            System.out.println("Erro de acesso ao arquivo");
        }
        
        while(GetArq.hasNext()){
            Type = GetArq.next();
            Peso = GetArq.next();
            Distancia = GetArq.next();
            Lucro = GetArq.next();
            peso = Integer.parseInt(Peso);
            distancia = Integer.parseInt(Distancia);
            lucro = Float.parseFloat(Lucro);
            Entrega New = new Entrega();
            New.setEntrega(Type, peso, distancia, lucro);
            newEntrega(New);
        }
            
    }
    
    /**
     *
     * @param NovaCarreta
     */
    public static void addCarreta(Carreta NovaCarreta){
        boolean Success;
        Success = Lista.add(NovaCarreta);
    }
    public static void addVan(Van NovaVan){
        boolean Success;
        Success = Lista.add(NovaVan);
    }

    /**
     *
     * @param NovoCarro
     */
    public static void addCar(Carro NovoCarro) {
        boolean Success;
        Success = Lista.add(NovoCarro);
    }
    
    /**
     *
     * @param NovaMoto
     */
    public static void addMoto(Moto NovaMoto) {
        boolean Success;
        Success = Lista.add(NovaMoto);
    }
    
    public static void newEntrega(Entrega Nova){
        boolean Success;
        Success = Delivery.add(Nova);
    }
    
}
