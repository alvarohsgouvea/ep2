/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

/**
 *
 * @author alvar
 */
public class Entrega {
    String veiculo;
    int peso;
    int distancia;
    float lucro;
    
    
    /////Setter
    public void setEntrega(String Veiculo, int Peso, int Distancia, float Lucro){
        this.veiculo = Veiculo;
        this.peso = Peso;
        this.distancia = Distancia;
        this.lucro = Lucro;
    }
    
    
    //////Getters
    public String getTipo(){
        return veiculo;
    }
    
    public int getPeso(){
        return peso;
    }
    
    public int getDistancia(){
        return distancia;
    }
    
    public float getLucro(){
        return lucro;
    }
}
